
import datetime
import urllib2
from bs4 import BeautifulSoup
from rfc3339 import rfc3339

class KEXPEvent:

    END_OF_ARTIST_DELIMETERS = ['session', 'LIVE']
    
    def __init__(self):
        self.headline = ""
        self.date = "TBD"
        self.broadcast_only = False
        self.open_to_public = False

    def __str__(self):
        return "%s on %s (%r, %r)" % (self.headline, self.date, self.broadcast_only, self.open_to_public)

    @property
    def title(self):
        """Return a custom event title, with syndication information
        prepending the artist name.
        Examples:
            PUBLIC: Tycho Live on KEXP
            BROADCAST: PUP Live on KEXP
        """
        public_open = 'PUBLIC' if self.open_to_public else 'BROADCAST'
        return "{public_open}: {artist} Live on KEXP".format(public_open=public_open, artist=self.artist)

    @property
    def artist(self):
        """Parse the full event headline, search for one of the
        valid delimeters between the artist name and the rest
        of the headline. Once found, returns the isolated artist name.
        """
        find_loc = None
        for delim in self.END_OF_ARTIST_DELIMETERS:
            # This find logic assumes that only one of the valid
            # delimeters will be present in a calendar event
            if delim not in self.headline:
                continue
            find_loc = self.headline.find(delim)
        return self.headline[:find_loc].strip()

    def google_cal_format(self):
        return {
            "summary": self.title,
            "location": "KEXP, 472 1st Ave N, Seattle, WA 98109",
            "description": "",
            "start": {
                "dateTime": rfc3339(self.date),
                "timeZone": "US/Pacific",
            },
            "end": {
                "dateTime": rfc3339(self.date + datetime.timedelta(hours=1)),
                "timeZone": "US/Pacific"
            },
            "reminders": {
                "useDefault": False,
                "overrides": [
                    {
                        "method": "popup",
                        "minutes": 60
                    },
                    {
                        "method": "popup",
                        "minutes": 4320     # 3 days
                    }
                ]
            }
        }



def get_events():
    response = urllib2.urlopen("http://kexp.org/events/instudio")
    soup = BeautifulSoup(response.read(), "html.parser")
    for element in soup.find_all("b"):  
        try:
            event = KEXPEvent()
            event.headline = element.contents[0].encode("utf-8").strip()
            event.date = datetime.datetime.strptime(str(element.next_sibling).strip(), "%A, %B %d, %Y, %I:%M %p")
            event.broadcast_only = "BROADCAST ONLY" in event.headline
            event.open_to_public = "OPEN TO THE PUBLIC" in event.headline
            yield event
        except ValueError as err:
             pass
        