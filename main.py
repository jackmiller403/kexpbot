
import argparse
import kexp
from kexp_calendar import *

def delete_all_events(calendar_name):
    try:
        calendar = KEXPCalendar(calendar_name)
        print("Deleting events...")
        calendar.delete_events()
        print("Done deleting events...")
    except Exception as err:
        print(err.message)


def main(calendar_name):
    print("Updating KEXP Calendar...")
    try:
        calendar = KEXPCalendar(calendar_name)
        now = datetime.datetime.now()
        for event in kexp.get_events():
            if (event.date > now) and (not calendar.has_event(event)):
                calendar.add_event(event)
                print("Added " + str(event))
    except Exception as err:
        print(err.message)
    finally:
        print("Done updating KEXP Calendar...")
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='KEXP Bot')
    parser.add_argument("--calendar", required=True, type=str, help="Name of the calendar to add events to.")
    parser.add_argument("--delete", action="store_true")
    args = parser.parse_args()

    if (args.delete):
        delete_all_events(args.calendar)
    else:
        main(args.calendar)
